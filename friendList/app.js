// need to install
// npm init
// npm install express --save
// npm install ejs --save
// npm body-parser --save
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static("public")); // link with style
app.set("view engine", "ejs"); // to link ejs as defult .ejs  

var Friends = ["Hemel", "Utshaw", "Hamza", "Jeba", "Noman"];

app.get("/", function(req, res){
    res.render("home");
});

app.get("/friends", function(req, res){
    
    console.log("In friend req");
    res.render("friends", {friends: Friends});
});

app.post("/addFriend", function(req, res){
    var newFriend = req.body.newFriend;
    Friends.push(newFriend);
    res.redirect("/friends");
});


app.get("*", function(req, res){
    res.send("<h1>You hit into Error Link</h1>");
});

app.listen(3000, function(){
    console.log("Sarver has Started");
});