var mongoose = require('mongoose');
mongoose.connect("mongodb://localhost/blog_demo");

var postSchema = mongoose.Schema({
    title: String,
    content: String
});
var Post = mongoose.model("Post", postSchema);

var userSchema = mongoose.Schema({
    email: String,
    name: String,
    posts: [postSchema]
});
var User = mongoose.model("User", userSchema);

// var newUser = new User({
//     email: "hello@gmail.com",
//     name: "hello Nayan Biswas"
// });
// newUser.posts.push({
//     title: "new title",
//     content: "new content"
// });
// newUser.save(function(error, user){
//     if(error){
//         console.log(error);
//     } else{
//         console.log(user);
//     }
// });


// var newPost = new Post({
//     title: "new POst",
//     content: "I create a new post"
// });
// newPost.save(function(error, post){
//     if(error){
//         console.log(error);
//     }   else{
//         console.log(post);
//     }
// });

User.findOne({name: "hello Nayan Biswas"}, function(error, user){
    if(error){
        console.log(error);
    }   else{
        user.posts.push({
            title: "Three things I really hate",
            content: "Voldemort. Voldemort"
        });
        user.save(function(error, user){
            if(error){
                console.log(error);
            }   else{
                console.log(user);
            }
        });
    }
});