// need json, express, ejs
var express = require('express');
var app = express();

app.get("/", function(req, res){
    res.send("<h1>Hi There</h1>");
});
app.get("/rep/:text", function(req, res){
    var TEXT = req.params.text;
    res.render("home.ejs", {text: TEXT});
    // here text is variable in home.ejs
});
app.get("*", function(req, res){
    res.send("YOU HIT ERROR LINK!!!");
});

app.listen(3000, function(){
    console.log("Sarver Started");
});