// npm init
// npm install express --save
// npm install ejs --save
// npm install request
var express = require('express');
var app = express();

var request = require('request');

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));

app.use(express.static("public")); // link with style
app.set("view engine", "ejs"); // to link ejs as defult .ejs 

var movies = [];
function printMovies(){
    movies.forEach(function(movie){ 
        console.log(movie.name);
   });
};

app.get("/", function(req, res){
    res.render("home");
});


app.post("/addMovie", function(req, res){
    var newMovie = req.body.movieName;
    //var str = "http://www.omdbapi.com/?t="+req.body.movieName;
    request("http://www.omdbapi.com/?t=str", function(error, response, body){
        if(!error && response.statusCode == 200){
            console.log(body);
        }
        console.log(newMovie + " error = " + error + " response.statusCode = " + response.statusCode);
    });
    movies.push(newMovie);
    res.redirect("/movie");
});

app.get("/movie", function(req, res){
    res.render("list", {movies: movies});
});

app.get("*", function(req, res){
    res.send("<h1>YOU HIT WRONG LINK</h1>");
});

app.listen(3000, function(){
    console.log("Sarver has Started");
});