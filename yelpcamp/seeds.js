var mongoose = require('mongoose'),
    Campgrounds = require('./models/campgrounds'),
    Comments = require('./models/comments');

var datas = [
    {
        name: "Scenic Rim",
        image: "http://www.scenicrim.qld.gov.au/documents/44179689/eb95e5ee-7154-43b1-9987-d6f77ce3189f",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
    },
    {
        name: "River Side",
        image: "https://img.sunset02.com/sites/default/files/styles/4_3_horizontal_-_1200x900/public/image/2016/10/main/hoodview-campground-0510.jpg?itok=xo0RuR6u",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
    },
    {
        name: "Maine Midcoast",
        image: "https://b1e2.https.cdn.softlayer.net/80B1E2/maine.bvk.geoconsensus.com/images/otc/input/content/500/w/vtm9D7B3A8ADC6EB9713.png",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
    }
];

function seedBD() {
    Campgrounds.remove({}, function (error) {
        if (error) {
            console.log(error);
        } else {
            console.log("Removed evry things from Campgrounds");
        }
    });
    Comments.remove({}, function (error) {
        if (error) {
            console.log(error);
        } else {
            console.log("Removed evry things from Comments");
        }
    });
    datas.forEach(function (data) {
        Campgrounds.create(data, function (error, campground) {
            if (error) {
                console.log(error);
            } else {
                Comments.create({
                    text: "This place is great, but I wish there was internet",
                    author: "Nayan Biswas"
                },
                    function (error, comment) {
                        if (error) {
                            console.log(error);
                        } else {
                            campground.comments.push(comment);
                            campground.save();
                            console.log("Creat a new comment");
                        }
                    }
                );

            }
        });

    });
};


module.exports = seedBD;