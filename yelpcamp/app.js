const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    LocalStrategy = require('passport-local'),
    passportLocalMongoose = require('passport-local-mongoose'),
    expressSession = require('express-session'),
    methodOverride = require('method-override'),
    flash = require('connect-flash');

const User = require('./models/user'),
    Campgrounds = require("./models/campgrounds"),
    Comments = require("./models/comments"),
    seedBD = require('./seeds'),
    authRouter = require('./routes/index'),
    campgroundsRouter = require('./routes/campgrounds'),
    commentsRouter = require('./routes/comments');
// seedBD(); // seed the database
mongoose.connect("mongodb://localhost/yelp_camp");
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static("public")); // link with style
app.set("view engine", "ejs"); // to link ejs as defult .ejs
app.use(methodOverride("_method"));
app.use(flash());
app.use(require('express-session')({
    secret: "you are the wrost developer",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(function (req, res, next) {
    res.locals.user = req.user;
    res.locals.error = req.flash("error");
    res.locals.success = req.flash("success");
    next();
});

app.use(authRouter);
app.use("/campgrounds", campgroundsRouter);
app.use("/campgrounds/:id/comments", commentsRouter);

app.get("*", function (req, res) {
    res.send("<h1>You Hit Wrong Link</h1>");
});

app.listen(3000, function () {
    console.log("Server has Started");
});