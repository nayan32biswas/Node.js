var Campgrounds = require('../models/campgrounds'),
    Comments = require('../models/comments');

var middlewareObj = {};

middlewareObj.campgroundOwnership = function (req, res, next) {
    if (req.isAuthenticated()) {
        Campgrounds.findById(req.params.id, function (error, findCampground) {
            if (error) {
                req.flash("error", "Campground not found");
                res.redirect("back");
            } else {
                if (findCampground.author.id.equals(req.user._id)) {
                    next();
                } else {
                    req.flash("error", "You don't have permission to do that");                    
                    res.redirect("back");
                }
            }
        });
    } else {
        req.flash("error", "You need to be logged in to do that.");
        res.redirect("back");
    }

}

middlewareObj.commentOwnership = function (req, res, next) {
    if (req.isAuthenticated()) {
        Comments.findById(req.params.comment_id, function (error, findComment) {
            if (error) {
                req.flash("error", "Comment not found");
                res.redirect("back");
            } else {
                if (findComment.author.id.equals(req.user._id)) {
                    next();
                } else {
                    req.flash("error", "You don't have permission to do that");
                    res.redirect("back");
                }
            }
        });
    } else {
        req.flash("error", "You need to be logged in to do that");
        res.redirect("back");
    }
}

middlewareObj.isLoggedIn = function (req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        req.flash("error", "You need to be logged in to do that");
        res.redirect("/login");
    }
}

module.exports = middlewareObj;