var express = require('express'),
    Router = express.Router({
        mergeParams: true
    }),
    Campgrounds = require('../models/campgrounds'),
    Comments = require('../models/comments'),
    middleware = require('../middleware/index');

Router.post("/", middleware.isLoggedIn, function (req, res) {
    Campgrounds.findById(req.params.id, function (error, campground) {
        if (error) {
            req.flash("error", "Comment not found");
            res.redirect("back");
        } else {
            Comments.create(req.body.comment, function (error, comment) {
                if (error) {
                    console.log(error);
                    res.redirect("/campgrounds");
                } else {
                    comment.author.id = req.user._id;
                    comment.author.username = req.user.username;
                    comment.save();
                    campground.comments.push(comment);
                    campground.save();
                    req.flash("success", "Successfully added comment");
                    res.redirect("/campgrounds/" + campground._id);
                }
            })
        }
    });
});
Router.get("/new", middleware.isLoggedIn, function (req, res) {
    Campgrounds.findById(req.params.id, function (error, campground) {
        if (error) {
            req.flash("error", "Comment not found");
            res.redirect("back");
        } else {
            res.render("comments/new", {
                campground: campground
            });
        }
    });
});
// Edit route
Router.get("/:comment_id/edit", middleware.isLoggedIn, middleware.commentOwnership, function (req, res) {
    Comments.findById(req.params.comment_id, function (error, findComment) {
        if (error) {
            req.flash("error", "Comment not found");
            res.redirect("back");
        } else {
            res.render("comments/edit", {
                campground_id: req.params.id,
                comment: findComment
            });
        }
    });
});
Router.put("/:comment_id/edit", middleware.isLoggedIn, middleware.commentOwnership, function (req, res) {
    Comments.findByIdAndUpdate(req.params.comment_id, req.body.comment, function (error, updatedComment) {
        if (error) {
            req.flash("error", "Comment not found");
            res.redirect("back");
        } else {
            req.flash("success", "Successfully edit comment");
            res.redirect("/campgrounds/" + req.params.id);
        }
    });
});
// Delet route
Router.delete("/:comment_id", middleware.isLoggedIn, middleware.commentOwnership, function (req, res) {
    Comments.findByIdAndRemove(req.params.comment_id, function (error) {
        if (error) {
            req.flash("error", "Comment not found");
            res.redirect("back");
        } else {
            req.flash("success", "Successfully comment deleted");
            res.redirect("back");
        }
    });
});

module.exports = Router;