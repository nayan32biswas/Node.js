var express = require('express'),
    Router = express.Router(),
    passport = require('passport'),
    User = require('../models/user');

Router.get("/", function (req, res) {
    res.render("home");
});

Router.get("/register", function (req, res) {
    res.render("register");
});
Router.post("/register", function (req, res) {
    var newUser = new User({
        username: req.body.username
    });
    User.register(newUser, req.body.password, function (error, user) {
        if (error) {
            req.flash("error", error.message);
            res.redirect("back");
        }
        passport.authenticate("local")(req, res, function () {
            req.flash("success", "Welcome to YelpCamp " + user.username);
            res.redirect("/campgrounds");
        });
    });
});

// ======================================
// show login form
// ======================================
Router.get("/login", function (req, res) {
    res.render("login");
});
Router.post("/login", passport.authenticate("local", {
    successRedirect: "/campgrounds",
    failureRedirect: "/login"
}), function (req, res) {});

// ======================================
// logout route
// ======================================
Router.get("/logout", function (req, res) {
    req.logout();
    req.flash("success", "Loged You Out!");
    res.redirect("/campgrounds");
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.redirect("/login");
    }
}

module.exports = Router;