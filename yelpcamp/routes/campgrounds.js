var express = require('express'),
    Router = express.Router(),
    Campgrounds = require('../models/campgrounds'),
    middleware = require('../middleware/index');

Router.get("/", function (req, res) {
    Campgrounds.find({}, function (error, allCampground) {
        if (error) {
            console.log(error);
        } else {
            res.render("campgrounds/index", {
                campgrounds: allCampground
            });
        }
    });
});

Router.post("/", middleware.isLoggedIn, function (req, res) {
    var name = req.body.name;
    var price = req.body.price;
    var image = req.body.image;
    var desc = req.body.description;
    var newCampgroud = {
        name: name,
        price: price,
        image: image,
        description: desc,
        author: {
            id: req.user._id,
            username: req.user.username
        }
    };
    // console.log("post is: ", newCampgroud);
    Campgrounds.create(newCampgroud, function (error, newlyCreated) {
        if (error) {
            consloe.log(error);
        } else {
            req.flash("success", "Successfully added campgroudn");
            res.redirect("/campgrounds");
        }
    });
});

Router.get("/new", middleware.isLoggedIn, function (req, res) {
    res.render("campgrounds/new");
});

Router.get("/:id", function (req, res) {
    Campgrounds.findById(req.params.id).populate("comments").exec(function (error, findCampground) {
        if (error) {
            console.log(error);
        } else {
            res.render("campgrounds/show", {
                campground: findCampground
            });
        }
    });
});


Router.get("/:id/edit", middleware.isLoggedIn, middleware.campgroundOwnership, function (req, res) {
    Campgrounds.findById(req.params.id, function (error, findCampground) {
        if (error) {
            console.log(error);
            res.redirect("/campgrounds/" + req.params.id);
        } else {
            res.render("campgrounds/edit", {
                campground: findCampground
            });
        }
    });
});
Router.put("/:id/edit", middleware.isLoggedIn, middleware.campgroundOwnership, function (req, res) {
    Campgrounds.findByIdAndUpdate(req.params.id, req.body.campground, function (error, updatedCampground) {
        if (error) {
            res.redirect("/campgrounds");
            console.log(error);
        } else {
            req.flash("success", "Successfully edit campgroudn");
            res.redirect("/campgrounds/" + req.params.id);
        }
    });
});


Router.delete("/:id", middleware.isLoggedIn, middleware.campgroundOwnership, function (req, res) {
    Campgrounds.findByIdAndRemove(req.params.id, function (error) {
        if (error) {
            req.flash("error", "Campground not found");
            res.redirect("/campgrounds");
        } else {
            res.redirect("/campgrounds");
        }
    });
});

module.exports = Router;