var mongoose = require('mongoose');
var campgroundsSchema = new mongoose.Schema({
    name: String,
    price: Number,
    image: String,
    description: String,
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Comments"
    }],
    author: {
        id: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        username: String
    }
});
module.exports = mongoose.model("Campgrounds", campgroundsSchema);