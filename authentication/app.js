// All requre thing are included
const express = require('express'),
    mongoose = require('mongoose'),
    passport = require('passport'),
    bodyParser = require('body-parser'),
    User = require('./models/user'),
    LocalStrategy = require('passport-local'),
    passportLocalMongoose = require('passport-local-mongoose');
// ================================================================


// conection setup
mongoose.connect('mongodb://localhost/authentication');
var app = express();
app.set("view engine", "ejs");

app.use(require('express-session')({
    secret: "Rusty is the best and cutest dog in the world",
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());

passport.use(new LocalStrategy(User.authenticate()));

passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(bodyParser.urlencoded({
    extended: true
}));

// ================================================================


// Normal routes
app.get("/", function (req, res) {
    res.render("home");
});
app.get("/secret", isLoggedIn, function (req, res) {
    res.render("secret");
});

// ================================================================


// regester page and action
app.get("/register", function (req, res) {
    res.render("register");
});
app.post("/register", function (req, res) {
    User.register(new User({
        username: req.body.username
    }), req.body.password, function (error, user) {
        if (error) {
            console.log(error);
            return res.render("register");
        } else {
            console.log(user);
            passport.authenticate("local")(req, res, function () {
                res.redirect("/secret");
            });
        }
    });
});
// ================================================================

// login page and action
app.get("/login", function (req, res) {
    res.render("login");
});
app.post("/login", passport.authenticate("local", {
        successRedirect: "/secret",
        failureRedirect: "/login"
    }),
    function (req, res) {
    });
// ================================================================

app.get("/logout", function(req, res){
    req.logout();
    res.redirect("/");
});

function isLoggedIn(req, res, next){
    if(req.isAuthenticated()){
        return next();
    }
    res.redirect("/login");
}

app.get("*", function (req, res) {
    res.send("<h1>You hit wrong link</h1>");
});
// local Server conection
app.listen(3000, function () {
    console.log("sarver has started.........");
});
// ================================================================